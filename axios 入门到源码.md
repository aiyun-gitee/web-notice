# axios

[axios官网](http://www.axios-js.com/)

## 什么是 axios？

Axios 是一个基于 promise 的 HTTP 库，可以用在浏览器和 node.js 中。

接口请求案例

```javascript
<body>
	<div id="app">
		<input type="text" v-model="inputValue"/>
		<button @click="getData">点击</button>
		<span>{{obj.name}} - {{obj.pinyin}}</span>
	</div>
</body>
<script>
	new Vue({
            el:'#app',
            data:{
                inputValue:'',
                obj:{
                    name:'',
                    pinyin:''
                }
            },
            methods:{
                getData(){
                    //发送axios请求，获取接口数据
                    url="https://api.52kfw.cn/v1/cities?type=" + inputValue
               		axios.get(url)
                    .then(data=>{
                        this.obj.name = data.data.name
                        this.obj.pinyin = data.data.pinyin
                        console.log(this.obj.name)
                    })
                    .catch(error=>{
                        console.log(error)
                    })
                }
            }
        })
</script>

```

