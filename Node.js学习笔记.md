# Node.js

# 1.Node.js介绍

## 1.1 Node.js 简介

Node.js 是一个开源与跨平台的 JavaScript 运行时环境。 它是一个可用于几乎任何项目的流行工具！

Node.js 在浏览器外运行 V8 JavaScript 引擎（Google Chrome 的内核）。 这使 Node.js 表现得非常出色。——

[Node.js官网]: http://nodejs.cn/

## 大量的库

npm 的简单结构有助于 Node.js 生态系统的激增，现在 npm 仓库托管了超过 1,000,000 个可以自由使用的开源库包。

## Node.js 应用程序的示例

Node.js 最常见的 Hello World 示例是 Web 服务器：

```
//引入了 Node.js http 模块。
const http = require('http')

const hostname = '127.0.0.1'
const port = 3000

//http 的 createServer() 方法会创建新的 HTTP 服务器并返回它
const server = http.createServer((req, res) => {
  //表明响应成功
  res.statusCode = 200
  //设置 Content-Type 响应头
  res.setHeader('Content-Type', 'text/plain')
  //关闭响应
  res.end('你好世界\n')
})

server.listen(port, hostname, () => {
  console.log(`服务器运行在 http://${hostname}:${port}/`)
})
```

# 2.前期准备

1. 安装node.js

2. 安装vscode编辑器

3. 下载vscode插件：node-snippets

   作用：代码提示

4. 安装淘宝镜像cnpm

   **安装命令：npm install -g cnpm --registry=https://registry.npm.taobao.org**

   作用：和npm一样的功能，只是较快

5. 安装自动监测更新重启服务的插件：（任选其一）

   （1）supervisor

   **安装命令：cnpm install -g supervisor**

   安装完后重启vscode，打开终端，切换到当前工作目录下，使用supervisor代替node命令，即：**supervisor 目录名**（如：supervisor server.js）

   **作用**：nodejs自启动工具，它会不停的watch你应用下面的所有文件，发现有文件被修改，就重新载入程序文件这样就实现了部署，修改了程序文件后马上就能看到变更后的结果，不用总是重启nodejs了

   （2）nodemon

   ### 设置淘宝镜像：

   ```
   安装镜像管理工具nrm ：
   npm install nrm -g
   查看：
   nrm test
   若报错：
   根据错误提示找到错误文件修改17行为：
   const NRMRC = path.join(process.env[(process.platform == 'win32') ? 'USERPROFILE' : 'HOME'], '.nrmrc');
   
   1.通过cnpm使用淘宝镜像：
   npm install -g cnpm --registry=https://registry.npm.taobao.org
   
   2.将npm设置为淘宝镜像：
   npm config set registry https://registry.npm.taobao.org
   
   3. 查看npm镜像设置：
   npm config get registry 
   
   4.查看cnpm镜像设置：
   cnpm config get registry 
   ```
   
   
   
   # 2.http和url模块
   
   
   
   # 3.CommonJs和Nodejs模块、自定义模块
   
   ## 3.1.CommonJs：
   
   ![image-20210804081722053](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210804081722053.png)
   
   CommonJS就是模块化的标准，nodejs就是CommonJS(模块化)的实现
   
   
   
   ## 3.2.Nodejs中的模块化
   
   Node应用由模块组成，采用CommonJS模块规范
   
   ![image-20210804082236894](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210804082236894.png)
   
   

![image-20210804082317908](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210804082317908.png)

![image-20210804094702344](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210804094702344.png)

