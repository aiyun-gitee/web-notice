# Vue学习笔记

# 1.Vue基础

## 1.1 Vue简介

### 1.1.1.官网

1. 中文官网：https://cn.vuejs.org/
2. 英文官网：https://vuejs.org/

加上如下两句可以随意折叠代码

//#regin

//#endregion

### 1.1.2.介绍与描述



## 1.2. 初识Vue

![image-20210730135906008](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210730135906008.png)

一套用于构建用户界面的渐进式JavaScript框架

构建用户界面：数据--->Vue--->界面

渐进式：指Vue可以自底向上逐层的应用

简单应用：只需一个轻量小巧的核心库

复杂应用：可以引入各式各样的Vue插件

​														——Vue官网

Vue特点：

1、采用组件化模式，提高代码复用率和可维护性（

在vue中一个.vue文件就是一个组件）

2、声明式编码，无需直接操作DOM（与之相对的是命令式编码）

3、使用虚拟DMO+优秀的Diff算法，尽量复用DOM节点

![image-20210730141652163](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210730141652163.png)



**安装：**

1、直接使用<script>直接引入

2、使用npm

开发版本：包含完整的警告和调试模式

生产版本：删除了警告



在使用 Vue 时，我们推荐在你的浏览器上安装 [Vue Devtools](https://github.com/vuejs/vue-devtools#vue-devtools)。它允许你在一个更友好的界面中审查和调试 Vue 应用。https://github.com/vuejs/vue-devtools#vue-devtools)

Vue官网-->学习-->教程-->安装-->Vue Devtools-->关于下边[devtools.vuejs.org](https://devtools.vuejs.org/)-->**Guide**--> Installation-->[Get the Chrome Extension](https://chrome.google.com/webstore/detail/vuejs-devtools/nhdogjmejiglipccpnnnanhbledajbpd)-->添加至Chrome

**插件：**

vscode中推荐使用的Vue插件：

1.代码提示插件：Vue3 snippets，作者：hollowtree



## 数据代理

Object.defineproperty方法

![image-20210817101631493](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210817101631493.png)

**何为数据代理**

![image-20210817101657046](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210817101657046.png)

**vue中的数据代理**

![image-20210817101735397](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210817101735397.png)

![image-20210817101803886](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210817101803886.png)

## 事件处理

### 事件的基本使用

![image-20210816153055102](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210816153055102.png)

例子：

![image-20210816153205299](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210816153205299.png)

![image-20210816153153015](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210816153153015.png)



### 事件修饰符

Vue中的事件修饰符 

![image-20210816153833998](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210816153833998.png)

例子：

1.阻止默认行为：e.preventDefault()

![image-20210816153632121](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210816153632121.png)

更便捷的写法：使用事件修饰符prevent

![image-20210816153751745](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210816153751745.png)



2.阻止事件冒泡

e.stopPropagation()

vue中.stop



3.事件的默认行为立即执行 .passive

鼠标滚轮滚动事件 @wheel 

滚动条滚动事件 @scroll 



4.修饰符可以连续写

![image-20210817110118223](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210817110118223.png)

### 键盘事件

@keyup.按键别名

@keydown.按键别名

@keycode.按键编号

1.常用的键盘事件

![image-20210817101836399](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210817101836399.png)

2.如何定义按键别名？

![image-20210817101854126](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210817101854126.png)

3.按键可以连写

![image-20210817110346821](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210817110346821.png)



## 计算属性-computed

![image-20210817144445427](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210817144445427.png)

例子：

![image-20210817143355241](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210817143355241.png)



![image-20210817142856194](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210817142856194.png)

![image-20210817144423854](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210817144423854.png)

计算属性简写1（只读时）

![image-20210817144817551](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210817144817551.png)

进一步简写：

![image-20210817144904124](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210817144904124.png)

### 

### 监视属性-watch

## 11 条件渲染

![image-20210819091310263](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210819091310263.png)

![image-20210819091221519](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210819091221519.png)

## 12 列表渲染

### 12.10 总结Vue数据监测

![image-20210818150305521](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210818150305521.png)



## 13收集表单数据

![image-20210818172954659](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210818172954659.png)

例子：

![image-20210818172611742](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210818172611742.png)

![image-20210818172412821](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210818172412821.png)



## 14 过滤器

![image-20210818195640317](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210818195640317.png)

例子：

![image-20210818194419574](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210818194419574.png)

![image-20210818194131365](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210818194131365.png

![image-20210818194153261](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210818194153261.png)



## 15 内置指令

![image-20210818200102821](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210818200102821.png)



## 16 自定义指令-directives

### 16.1自定义指令

![image-20210819084905531](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210819084905531.png)





### 16.2 回顾一个DOM操作

所有指令相关的this都是window

例子：

1.定义局部指令：

![image-20210819084254359](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210819084254359.png)

![image-20210819084231471](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210819084231471.png)

2.定义全局指令

![image-20210819084700474](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210819084700474.png)



## 17 生命周期

### 17.1 引出生命周期

![image-20210819090431574](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210819090431574.png)

例子：

![image-20210819090039774](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210819090039774.png)

![image-20210819090025614](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210819090025614.png)



template

![image-20210819095225395](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210819095225395.png)

总结：

![image-20210820085224802](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210820085224802.png)





# 第二章 Vue组件化编程

## 2.1 模块与组件、模块化和组件化

传统方式编写应用

![image-20210820093345613](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210820093345613.png)



使用组件方式编写应用

![image-20210820093917615](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210820093917615.png)

![image-20210820094033422](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210820094033422.png)

![image-20210820094048375](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210820094048375.png)



## 2.2 非单文件组件

一个文件中包含有n个组件

### 2.2.1 基本使用

![image-20210820114832350](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210820114832350.png)

第一步：创建组件

![image-20210820114251215](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210820114251215.png)

第二步：注册组件

1.局部注册：

![image-20210820114620108](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210820114620108.png)

2.全局注册

![image-20210820114550490](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210820114550490.png)

第三步：应用组件

![image-20210820114201744](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210820114201744.png)

### 2.2.2 几个注意点

![image-20210820140139674](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210820140139674.png)

### 2.2.3 组件的嵌套

### 2.2.4 VueComponent

### 2.2.5 一个重要的内置关系

![image-20210820202724775](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210820202724775.png)

## 2.3 单文件组件

一个文件中只包含有1个组件

## 2.4脚手架

`cmd`命令行：

`cd` 目录名 ==> 切换目录 

`mkdir` 文件名 ==> 创建文件夹 

`cls` ==> 清屏 

**设置淘宝镜像：**

```
1.通过cnpm使用淘宝镜像：
npm install -g cnpm --registry=https://registry.npm.taobao.org

2.将npm设置为淘宝镜像：
npm config set registry https://registry.npm.taobao.org

3. 查看npm镜像设置：
npm config get registry 

4.查看cnpm镜像设置：
cnpm config get registry 
```

**其他可选安装项：**

查看webpack 版本所有 `npm view vue webpack versions`

安装less7 `npm i less-loader@7`

使用 `<style lang="less">`

安装id生成工具 `npm i nanoid`

唯一id生成库：uuid、nanoid

### 创建vue脚手架项目

`vue create vue_test` 项目名==>创建vue脚手架项目

 ![image-20210821151143395](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210821151143395.png)

选择vue版本，这里选vue2，并按下回车即可（babel是ES6转ES5用的，eslint是做语法检查），`cd 文件名`进入该文件目录下并运行

![image-20210821153931015](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210821153931015.png)

运行后：

![image-20210821153815354](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210821153815354.png)

如何停止？按两次 `Ctrl + c`

![image-20210821153645239](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210821153645239.png)

使用vscode打开刚刚创建的vue_test项目

分析项目结构：

![image-20210821164411622](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210821164411622.png)

public：

favicon.ico:页签图标

src:

assets：静态资源文件夹

main.js 整个项目的入口文件，运行vue run serve，首先进入该文件

![image-20210821155926773](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210821155926773.png)

.gitignore：git忽略文件：哪些文件不想交给git管理，就在这配置

babel.config.js：ES6=>ES5配置文件，一般无须自己配置

package-lock.json：包版本控制文件

package.json：包的说明

README.md：整个工程/项目的说明



关于不同版本的vue

![image-20210821170957418](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210821170957418.png)

### 2.4.1 混入（合）-mixin

例子：

定义混入：![image-20210823092039116](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210823092039116.png)

引入混入：`import {mixin,mixin2} from '../mixin'`

使用混入：局部混入：`mixins:[mixin,mixin2]`

​				    全局混入：`Vue.mixin(mixin)`

​										`Vue.mixin(mixin2)`

### 2.4.2 自定义插件

![image-20210823093409542](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210823093409542.png)

![image-20210830090059502](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210830090059502.png)

使用插件

import xxx from './xxx'

Vue.use(xxx)

### 总结TodoList案例

![image-20210901201432078](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210901201432078.png)



### 浏览器本地存储

![image-20210902112800402](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210902112800402.png)

### 组件的自定义事件

![image-20210902172540314](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210902172540314.png)

### 全局事件总线

![image-20210902194957108](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210902194957108.png)

### 消息订阅与发布

使用pubsub-js（publish subscribe-js) 库可以在任何框架实现消息订阅与发布

安装 `npm i pubsub-js`

![image-20210903090838888](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210903090838888.png)

### nextTick

![image-20210903100310890](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210903100310890.png)

### 动画效果 & 过度效果



集成的第三方动画库：https://animate.style/

![image-20210903111241733](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210903111241733.png)

![image-20210903111435582](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210903111435582.png)



### 配置代理

`npm i axios`

![image-20210904091043851](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210904091043851.png)

### vue-resource

发送Ajax请求方式：

1. xhr	

2. jQuery

3. fetch

4. axios

5. vue-resource

   

使用 vue-resource 发送Ajax请求

`npm i vue-resource`



安装路由：`npm i vue-router`

`npm run serve` 开启8080内置服务器，支撑其运行

`npm run build` 将代码转为纯粹的html、css、js



### 项目上线的基本流程

将静态资源打包放进服务器

1. 打包：`npm run build` （路由工作模式为 hash 或 history ）

2. 开启一个服务器，这里使用node.js开启一个服务器

![image-20210907144036195](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210907144036195.png)

3. 解决使用路由 history 工作模式部署在服务器上后刷新出现 404 问题

   下载：`npm i connect-history-api-fallback`

   引入：`const history **=** require('connect-history-api-fallback');`

   使用：`app.use(history())`

4. 其他方式：也可以使用 Nginx 解决，使用Nginx代理来分辨是前端路由还是后端路由



### element-ui 按需引入

element-ui 按需引入时：babel.config.js 配置文件中 `es2015` 要改为 `@babel/preset-env`

![image-20210907153834746](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210907153834746.png)

