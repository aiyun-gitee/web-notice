# JavaScript高级

## 1. 基础总结

### 1.1. 数据类型

![image-20210807172415595](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210807172415595.png)

1. ##### undefined与null的区别?

   - undefined代表定义但未赋值，null是定义了并赋值为null。

   ![image-20210806213507863](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210806213507863.png)

2. ##### 什么时候给变量赋值为null呢？

   - 初始赋值，表明要将其赋值为对象

   - 结束前赋值，让对象成为垃圾对象（被垃圾回收器回收）

3. ##### 严格区别变量类型与数据类型？

   - 数据的类型：基本类型、对象类型

   - 变量的类型（变量内存值的类型）：基本类型（保存的就是基本类型的数据）、引用类型（保存的是地址值）
   
     

### 1.2. 数据、变量、内存

1. ##### 什么是数据？

   - 存储在内存中代表特定信息的东西 ，本质上是0101...
   - 数据的特点：可传递、可运算
   - 一切皆数据
   - 内存中所有操作的目标：数据
     - 算术运算、逻辑运算、赋值运算、运行函数

2. ##### 什么是内存？

   - 内存条通电后产生的可存储数据的空间（临时的）

   - 内存产生和死亡：内存条（电路板）==>通电==>产生内存空间==>存储数据==>处理数据==>断电==>内存空间和数据都消失
   - 一块小内存的2个数据：
     - 内部存储的数据
     - 地址值
   - 内存分类：
     - 栈：存储全局变量、局部变量
     - 堆：对象

3. ##### 什么是变量？

   - 可变化的量，由变量名和变量值组成
   - 每个变量都对应的一块小内存，变量名用来查找对应的内存，变量值就是内存中保存的数据

4. ##### 内存、数据、变量三者之间的关系

   内存是用来存储数据的空间， 变量是内存的标识

5. ##### 关于引用变量赋值问题

   - 2个应用变量指向同一个对象，通过一个变量修改对象内部数据，其他所有变量看到的是修改之后的数据

   - 2个引用变量指向同一个对象，让其中一个引用变量指向另一个对象，另一引用变量依然指向前一个对象

6. ##### 在JS中调用函数传递变量参数时，是值传递还是引用传递？

   - 理解1：都是值传递（基本值/地址值）
   - 理解2：可能是值传递，也可能是引用传递（地址值）

7. ##### JS引擎如何管理内存？

    - 内存生命周期
      - 分配小内存空间，得到它的使用权
      - 存储数据，可以反复进行操作
      - 释放小内存空间
      - ![image-20210807110228544](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210807110228544.png)

    **释放内存**

    - 局部变量：函数执行完自动释放

    - 对象：成为垃圾对象==>垃圾回收器回收

      

### 1.3. 对象

1. ##### 什么是对象？

   - 代表现实中的某个事物，是该事物在编程中的抽象

   - 多个数据的封装体

   - 用来保存多个数据的容器
   - 一个对象代表现实中的一个事物

2. ##### 为什么要用对象？

   - 便于对多个数据进行统一管理

3. ##### 对象的组成

   - 属性：
     - 代表显示事物的状态数据
     - 由属性名和属性值组成
     - 属性名都是字符串类型，属性值是任意类型
   - 方法：
     - 代表现实事物的行为数据
     - 是一种特别的属性（属性值是函数）

4. ##### 如何访问对象内部数据？

   - 对象名.属性名 ：编码简单，有时不能用
   - 对象名[属性名] ：编码复杂，但通用

5. ##### 什么时候必须使用[‘属性名’]的方式

   - 属性名包含特殊字符：- 空格

   - 属性名不确定

     ![image-20210807111739092](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210807111739092.png)

     


### 1.4. 函数

1. ##### 什么是函数？

   - 实现特定功能的n条语句的封装体
   - 只有函数是可以执行的，其他类型的数据不能执行

2. ##### 为什么要用函数？

   - 提高代码复用
   - 便于阅读交流

3. ##### 如何定义函数？

   - 函数声明
   - 函数表达式

4. ##### 如何调用（执行）函数？

   - test()：直接调用
   - obj.test()：通过对象调用
   - new test()：new调用
   - test.call/apply(obj)：临时让test成为obj的方法进行调用

5. ##### 什么函数是回调函数？

    - 你定义的
    - 你没有调
    - 但最终它执行了（在某个时刻或某个条件）

6. ##### 常见的回调函数？

    - dom事件回调函数==>this是发生事件的dom元素

      ```js
      document.getElementById("btn").onclick = function(){
      	alert(this.innerHTML);
      };
      <button>按钮</button>
      ```

    - 定时器回调函数==>this是window

      ```js
      //定时器：超时定时器、循环定时器
      setTimeout(function(){
          alert('点到了')
      },2000);
      ```

    - ajax请求回调函数

    - 生命周期回调函数

7. ##### IIFE（Immediately-Invoked Function Expression）

    ![image-20210807150832116](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210807150832116.png)

    - 理解：匿名函数自调用

    - 作用：

      - 隐藏实现

      - 不会污染外部（全局）命名空间

      - 用它来编写js模块

        

#### 1.4.1. this

1. ##### 什么是this?

    - 任何函数本质上都是通过某个对象来调用的，如果没有直接指定就是window
    - 所有函数内部都有一个变量this
    - 它的值是调用函数的当前对象

2. ##### 如何确定this的值？

    - test()：window

    - p.test()：p

    - new test()：新创建的对象

    - p.call(obj)：obj

      
#### 1.4.2. 分号

1. ##### JS中一条语句的后面加不加分号？

    是否加分号是编码风格的问题，没有应不应该，只有你自己喜不喜欢

    - 在下面2中情况下不加分号会有问题
      - 小括号开头的前一条语句
      - 中方括号开头的前一条语句

    - 解决办法：在行首加分号

    - 强有力的例子：vue.js库

    - 知乎热议：https://www.zhihu.com/question/20298345



## 2. 函数高级

### 2.1. 原型与原型链

1. 函数的prototype属性

   - 每个函数都有一个prototype属性，它默认指向一个Object空对象（即称为：原型对象）

   - 原型对象中有一个属性constructor，它指向函数对象

2. 给原型 对象添加属性（一般都是方法）

   - 作用：函数的所有实例对象自动拥有原型中的属性（方法）

     ![image-20210809084957198](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210809084957198.png)

3. 

#### 2.1.1. 原型（prototype）

#### 2.1.2. 显示原型与隐式原型

1. 每个函数function都有一个prototype，即显式原型（属性）
2. 每个实例对象都有一个__ proto __ ,可称为隐式原型
3. 对象的隐式原型的值为其对应构造函数的显式原型的值
4. 内存结构
5. 总结：
   - 函数的prototype属性：在定义函数时自动添加的，默认值是一个空Object对象
   - 对象的__ proto __属性：创建实例对象时自动添加，默认值为构造函数的prototype属性
   - 程序员能直接操作显式原型，但不能直接操作隐式原型（ES6之前）

![image-20210809091739899](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210809091739899.png)

![image-20210809091816624](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210809091816624.png)

#### 2.1.3. 原型链

![image-20210809094414393](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210809094414393.png)

![](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210809095047277.png)

##### 2.1.3.1. 原型链——属性问题

1. 读取对象的属性值时，会自动到原型链中查找
2. 设置对象的属性值时，不会查找原型链，如果当前对象中没有此属性，直接添加此属性并设置其值
3. 方法一般定义在原型中，属性一般通过构造函数定义在对象本身上

#### 2.1.4. 探索instanceof

1. instanceof是如何判断的？
   - 表达式：实例对象A instanceof 构造函数B
   - 如果B函数的显示原型对象在A对象的原型链上，返回true，否则返回false
2. Function是通过new自己产生的实例

#### 2.1.5. 面试题





### 2.2. 执行上下文与执行上下文栈

#### 2.2.1. 变量提升与函数提升

![image-20210809110240291](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210809110240291.png)



#### 2.2.2. 执行上下文

##### 2.2.2.1. 全局执行上下文

##### 2.2.2.2. 函数执行上下文

![image-20210809111634790](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210809111634790.png)

函数执行上下文是在函数调用的时候产生

#### 2.2.3. 执行上下文栈

![image-20210809140944936](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210809140944936.png)

#### 2.2.4. 面试题

1. ![](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210809105750693.png)

2. ![image-20210809145025383](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210809145025383.png)

    

3. ![image-20210809145114836](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210809145114836.png)

4. ![image-20210809145211285](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210809145211285.png)

    ​												相当于

    ![image-20210809145434950](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210809145434950.png)

 #### 2.3. 作用域与作用域链

##### 2.3.1. 作用域

    ![image-20210809164650656](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210809164650656.png)

##### 2.3.2. 作用域与执行上下文

![image-20210809165607242](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210809165607242.png)

##### 2.3.3. 作用域链

![image-20210809171107145](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210809171107145.png)



![image-20210809171708301](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210809171708301.png)



##### 2.3.4. 面试题

1. ![image-20210809171906499](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210809171906499.png)
2. ![image-20210809172535164](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210809172535164.png)



### 

### 2.4. 闭包

#### 2.4.1. 闭包的理解

![image-20210809174232204](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210809174232204.png)



![image-20210809173501293](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210809173501293.png)

将for循环的代码改为如下内容：

解决方法1：

![image-20210809173623930](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210809173623930.png)



解决方法2：

![image-20210809174044990](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210809174044990.png)

#### 2.4.2. 常见的闭包

![image-20210810084336775](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210810084336775.png)

第一种情况：

![image-20210810085744521](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210810085744521.png)

第二种情况：

![image-20210810090119651](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210810090119651.png)



#### 2.4.3. 闭包的作用

![image-20210810092443802](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210810092443802.png)

#### 2.4.4. 闭包的生命周期

![image-20210810093329070](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210810093329070.png)

#### 2.4.5. 闭包的应用

![image-20210810093035004](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210810093035004.png)

1. ![image-20210810100334458](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210810100334458.png)

   

   ![image-20210810100411422](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210810100411422.png)

   

2. ![image-20210810100315360](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210810100315360.png)



![image-20210810100444967](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210810100444967.png)



#### 2.4.6. 闭包的缺点及解决

![image-20210810111940370](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210810111940370.png)



#### 2.4.7. 内存溢出与内存泄漏

1. ![image-20210810112528686](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210810112528686.png)

   

   ![image-20210810112735786](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210810112735786.png)

   

   ![image-20210810112506627](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210810112506627.png)

   

2. ![image-20210810112613976](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210810112613976.png)

- ![image-20210810113608365](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210810113608365.png)



- ![image-20210810113258703](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210810113258703.png)

- ![image-20210810113411371](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210810113411371.png)



#### 2.4.8. 面试题

1. 没有闭包，没有引用外部变量（函数），object.getNameFunc()(),第一个括号（函数调用）结果为function（）{

   ​	return this.name;

   }()，此时的第二个（）是直接调用的，所以它的this指向window

   ![image-20210810114016968](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210810114016968.png)

   

2. 有闭包，此时的this被保存在that中，指向的是object2

   ![image-20210810114137514](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210810114137514.png)

   

3. 有闭包：闭包值为o

   ![image-20210810140139530](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210810140139530.png)

   结果为：
   
   ![image-20210810221334072](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210810221334072.png)





## 3. 面向对象高级

### 3.1. 对象创建模式

#### 3.1.1. Object构造函数模式

![image-20210810140727386](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210810140727386.png)

例如：

![image-20210810141025258](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210810141025258.png)



#### 3.1.2. 对象字面量模式

![image-20210810141207444](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210810141207444.png)



#### 3.1.3. 工厂模式

![image-20210810141721323](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210810141721323.png)

例如：

![image-20210810141700774](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210810141700774.png)



3.1.4. 自定义构造函数模式

![image-20210810142015225](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210810142015225.png)

例如：

![image-20210810142330059](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210810142330059.png)



#### 3.1.5. 构造函数+原型的组合模式

![image-20210810142409093](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210810142409093.png)

例如：

![image-20210810142429603](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210810142429603.png)



### 3.2. 继承模式

#### 3.2.1. 原型链继承

![image-20210810142953730](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210810142953730.png)

例如：

1.

![image-20210810220357936](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210810220357936.png)

2.

![image-20210810150511469](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210810150511469.png)



![image-20210810154936616](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210810154936616.png)



![image-20210810151501642](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210810151501642.png)

#### 3.2.2. 借用构造函数继承



![image-20210810155149476](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210810155149476.png)

例如：

1.

![image-20210810220429770](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210810220429770.png)

2.

![image-20210810155313973](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210810155313973.png)

#### 3.2.3. 原型链+借用构造函数的组合继承

![image-20210810155327273](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210810155327273.png)

例如：

1.

![image-20210810220530062](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210810220530062.png)

2.

![image-20210810155629681](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210810155629681.png)

![image-20210810155554984](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210810155554984.png)



![image-20210810220547280](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210810220547280.png)

## 4. 线程机制与事件机制

### 4.1. 进程与线程



### 4.2. 浏览器内核

![image-20210811185827307](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210811185827307.png)

### 4.3. 定时器引发的思考

![image-20210811110906891](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210811110906891.png)

例如：

![image-20210811122808833](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210811122808833.png)

### 4.4. JS是单线程执行的

![image-20210811185903169](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210811185903169.png)

![image-20210811122725470](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210811122725470.png)

例如：

1.

![image-20210811123128887](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210811123128887.png)

2.![image-20210811123303675](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210811123303675.png)

### 4.5. 浏览器的事件循环（轮询）模型

模型原理图

![image-20210811150027514](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210811150027514.png)



相关重要概念





![image-20210811151252333](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210811151252333.png)



事件处理机制

![image-20210811210002083](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210811210002083.png)

### 4.6. H5 Web Workers（多线程）

![image-20210811165335461](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210811165335461.png)



![image-20210811210157502](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210811210157502.png)

例如：

未使用前

![image-20210811170203465](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210811170203465.png)



使用web workers:

主线程：

![image-20210811171703185](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210811171703185.png)



分线程：

![image-20210811172511135](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210811172511135.png)





浏览器插件：

![image-20210811103706312](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210811103706312.png)
