# 02-HotelWebsite 项目笔记

# 1 项目简介

- 项目介绍：一个关于酒店名宿旅游行业的网站

- 功能模块：
  - 前台：
    - 网站首页、关于我们、优选房源、新闻动态、客房展示、五星团队、品牌介绍、联系我们
  - 后台：
    - 网站管理、分类管理、内容管理、客房管理

- 技术栈：

  - express、mongoose、body-parser、art-template、layui

## 1.1 项目文件结构

![image-20210827101421126](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210827101421126.png)



# 2 需求分析

## 2.1 首页模块

### 2.1.1首页轮播图

![image-20210827102414304](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210827102414304.png)

## 2.2 新闻动态

![image-20210827102709776](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210827102709776.png)

## 2.3 基本信息

![image-20210827102750038](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210827102750038.png)





# 3 项目后台开发实操

## 3.1项目初始化

1. 新建项目文件夹 `02-HotelWebsite`

2. 打开 cmd

3. 初始化文件夹： `npm init -y`

   **注意：**以下如果下载速度慢可以先设置一下npm的淘宝镜像：`npm config set registry https://registry.npm.taobao.org`

4. 下载 express 框架： `npm install express` 搭建服务端

5. 下载 nodemon：`npm install -g nodemon`更新后自动重启服务

6. 下载 mongoose： `npm install mongoose` 操作数据库

7. 安装`npm install body-parser` 解析post数据

8. 安装 art-template：`

   ```
   npm install --save art-template
   npm install --save express-art-template
   ```

   `

9. 文件上传安装express提供的：`npm install --save multer`

10. 后台模板使用的是 x-admin

11. 因为尽量模仿真实开发，所以生产环境和开发环境使用的数据库是不一样的，这里使用了`process.env.NODE_ENV` 设置了一下，具体设置为 package.json 文件中的 

    ```
    "dev": "set NODE_ENV=development&&nodemon app.js",
    "start": "set NODE_ENV=producation&&nodemon app.js",
    ```

    所以运行命令为 `npm run dev`

## 3.2 基本信息模块

## 3.3 新闻动态

### 3.3.1 新闻分类

每一条新闻信息都是要属于某一个分类的，所以在添加新闻之前，我们应该先完成新闻分类模块

1. 先设计新闻分类表

2. 代码完成curd操作

   步骤：

   1. 定义路由：news.js

      ```js
      const express = require('express')
      const router = express.Router()
          // 1.新闻的分类添加
      router.get('/category/add', (req, res) => {
          res.render('category-add', { title: '新闻的分类添加' })
      })
      
      // 2.新闻的分类展示
      router.get('/category/list', (req, res) => {
          res.render('category-list', { title: '新闻的分类展示' })
      })
      
      module.exports = router;
      ```

   2. 注册路由：app.js

      ```js
      const newsRouter = require('./routes/news.js')
      app.use('/admin', newsRouter);
      ```

   3. 

# 4 项目前台开发实操





此次项目实践，综合了前一阶段所学的知识，包括但不限于HTML、CSS、MongoD & mongoose、Node.js & Express等等，通过本次项目实践，巩固了之前所学，也从项目中遇到的问题当中学到了其他知识，提高了自己的编码能力。



