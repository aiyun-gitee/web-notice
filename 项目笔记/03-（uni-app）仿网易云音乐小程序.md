# uni-app网易云音乐项目实战

![image-20210912163347101](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210912163347101.png)

- 项目的 github地址：https://github.com/Binaryify/NeteaseCloudMusicApi

- 项目的设计图？
  - 通过手机截图 微信小程序《网易云音乐听歌》

- 项目的后端数据接口：https://github.com/Binaryify/NeteaseCloudMusicApi

- uniApp项目开发软件？
  - HBuilderX下载地址：https://www.dcloud.io/hbuilderx.html

  - 微信小程序开发者工具：https://developers.weixin.qq.com/miniprogram/dev/devtools/download.html

    json数据格式化工具：FeHelper

**项目步骤：**

1.  打开 Git Bash Here 下载项目的后端数据接口资源：

   `git clone https://github.com/Binaryify/NeteaseCloudMusicApi`

2. 进入刚刚下载的目录，打开cmd，下载依赖： `cnpm i`

3. 启动命令：`npm start`