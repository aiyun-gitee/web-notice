# JavaScript学习笔记

# 什么是JavaScript

JavaScript 是一门用来与网页交互的脚本语言，包含以下三个组成部分。 

1. ECMAScript：由 ECMA-262 定义并提供核心功能。

2. 文档对象模型（DOM）：提供与网页内容交互的方法和接口。 

3. 浏览器对象模型（BOM）：提供与浏览器交互的方法和接口。

   **数据类型：**

   - 基本数据类型：String、Number、Boolean、Undefined、Null

   - 引用数据类型：Oject、Function、Array

   **判断：**

   - typeof：可以判断undefined、number、string、Boolean、function；不能判断：null与object、object与array

   - instanceof：判断对象的具体类型

   - ===：可以判断undefined、null

# HTML 中的 JavaScript 

## 函数

```
// 1.最标准的函数定义
function haha(){

}

// 函数调用: 
haha()


// 2.匿名函数: 没有名字的函数
function () {
  
}

// 调用: 一般是使用事件驱动函数调用:
window.onload = function (){
  
}

// 3. 箭头函数:
// 3.1有返回值,没有参数的箭头函数: 
()=>{return a*2}

// 3.2有参数, 没有返回值的箭头函数: 
// 有多个参数: 
(a,b,c,d,e,...)=>{}

// 有一个参数的,可以写成两种格式: 
(a)=>{}
a=>{}

// 3.3有参数, 有返回值的箭头函数: 
(a)=>{return a}
// 或: 
a=>a

// 3.4没有参数, 但是有返回值
()=>{
  var result = 3*4
  return result
}

()=>3*4
```



# DOM

## dom在css中的应用

### 使用dom修改css样式

语法：**元素.style.样式名 =样式值**

注意：

​	1.如果css的样式名中含有-，使用驼峰命名（比如：background-color应写成backgroundColor)；

​	2.通过style修改的是内联样式，优先级较高，但是如果有!import,则无法通过style修改。

### 读取元素的内联样式

语法：**元素.style.样式名**

注意：只能读取内联样式，无法读取样式表中的样式

### 读取当前元素正在显示的样式(只读)

**IE浏览器：**使用 **currentStyle**

语法：**元素.currentStyle.样式名**

注意：	

​	1.只有IE浏览器支持

​	2.如果当前元素没有样式，则获取默认样式

**其他浏览器：**使用window方法：**getComputedStyle()**

```javascript
<html>
<head>
<style>
	.box1{
		width:100px;
		height:100px;
		background-color:red;
	}
</style>
</head>
<body>
<div class="box1"></div>
<script>
    var obj = getComputedStyle(box1,null);
    alert(obj.width);
</script>
<body>
</html>
```

需要两个参数：

第一个：要获取样式的元素

第二个：可以传递一个伪元素，一般都传null

该方法会返回一个对象，对象中封装了当前元素的对应样式，可以通过对象.样式名来读取样式。

如果没有设置width,不会获取到auto（或默认值），而是一个长度

**兼容IE和其他浏览器**：

```javascript
/*
定义一个参数，用来获取指定元素的当前样式
	参数：obj 要获取样式的元的
		 name 要获取的样式名
*/
function getStyle(obj,name){
    if(window.getComputedStyle){
    //正常浏览器的方式
    	return getComputedStyle(box1,null)[name];
    }else{
    //IE8的方式,没有getComputedStyle
    	return obj.currentStyle[name];
    	}
}
```

### 其他样式操作的属性（只读）

- **element.clientWidth、element.clientHeight** 获取元素的可见宽度和高度：（包括内容区和内边距，不包括边框）

- **offsetWidth、offsetHeight **返回元素的宽度和高度：（包括内容区、内边距和边框）

- **offsetParent **获取当前元素的开启定位的祖先元素，如果所有的祖先元素都没有开启定位，则返回body

- **offsetLeft、offsetTop** 分别获取当前元素相对于其定位父元素的水平/垂直偏移量

- **scrollLeft、scrollTop** 分别获取水平/垂直滚动条的距离

当满足scrollHeight - scrollTop == clientHeight ，说明垂直滚动条滚动到底了。

当满足scrollWidth - scrollLeft == clientWidth ，说明垂直滚动条滚动到底了。



# 事件

事件分为如下三个阶段

![image-20210805155209210](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210805155209210.png)



![image-20210805155643964](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210805155643964.png)



# BOM

BOM浏览器对象模型，可以使我们通过JS来操作浏览器

![image-20210805173637006](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210805173637006.png)

![image-20210805173719173](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210805173719173.png)

判断是什么浏览器

![image-20210805193619400](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210805193619400.png)



# JSON

![](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210806172433407.png)

![image-20210806172656325](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210806172656325.png)

![image-20210806172856690](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210806172856690.png)

json-->js对象

![image-20210806173224243](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210806173224243.png)

![image-20210806174415472](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210806174415472.png)

![image-20210806174909137](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210806174909137.png)
