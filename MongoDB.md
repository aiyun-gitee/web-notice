# MongoDB 

# 1 前期准备

SQL

- 结构化查询语言

- 关系数据库全都用SQL来操作

1.安装MongoDB（[菜鸟教程](https://www.runoob.com/mongodb/mongodb-window-install.html)）

2.配置环境变量

​	D:\software\database\MongoDB\Server\5.0\bin

3.在D盘根目录

​	创建一个文件夹 data

​	在 data 中创建一个文件夹 db

4.打开cmd窗口

​	输入 mongo 启动 MongoDB 服务器

5.在打开一个cmd窗口

​	输入 mongo  连接 MongoDB ，出现 >

6.（非必要项）若要修改存放目录：

​	mongod --dbpath 数据库路径D:\data\db 

7.（非必要项）默认端口27017，若要修改则执行以下命令：

​	mongod --dbpath 数据库路径(D:\data\db) --prot 端口号

​	端口号最大不要超过65535

- 数据库（database）

  - 数据库的服务器

    - 服务器用来保存数据

    - mongod 用来启动服务器
  
  - 数据库的客户端
    - 客户端用来操作服务器，对数据进行增删改查的操作
    - mongo 用来启动客户端

-  将 MongoDB 设置为系统服务，可以自动在后台启动，不需要每次都手动启动

1. 在D盘根目录创建文件夹data，在data下创建db和log文件夹

2. 创建配置文件，在目录D:\software\database\MongoDB\Server\5.0 下添加一个配置文件mongod.cfg

   mongod.cfg文件内容为：

```mongod.cfg文件
systemLog:
    destination: file
    path: d:\data\log\mongod.log
storage:
    dbPath: d:\data\db
```

3. 以管理员的身份打开命令行窗口

4. 执行如下命令：

   **安装 MongoDB服务:**

   通过执行mongod.exe，使用--install选项来安装服务，使用--config选项来指定之前创建的配置文件。

```
D:\software\database\MongoDB\Server\5.0\bin\mongod.exe --config "D:\software\database\MongoDB\Server\5.0\mongod.cfg" --install
```

5. 启动MongoDB服务：`mongo`
6. 如果启动失败，证明上边的操作有误：在控制台输入`sc delete MongoDB` 删除之前配置的服务，然后从第一步再来一次
- 基本概念

  数据库（database）

  集合（collection）

  文档（document）

  ​	在MongoDB中，数据库和集合都不需要手动创建

  ​		当我们创建文档时，如果文档所在的集合或数据库不存在会自动创建数据库和集合


- 基本指令

  `show dbs`

  `show database`

  ​	- 显示当前的所有数据库

  `use 数据库名`
  
  ​	- 进入到指定的数据库中
  
  `db`
  
  ​	-db表示当前所处的数据库
  
  `show collections`
  
  ​	- 显示数据库中所有的集合

- 数据库的CRUD（增删改查）的操作

   - 向数据库中插入文档

     ​	`db.<collection>.insert(doc)`

       - 向集合中插入一个文档

       - 例子，向test数据库中的，stus集合中插入一个新的学生对象{name:"张三",age:18,gender:"男"}

         `db.stus.insert({name:"张三",age:18,gender:"男"})`

  `db.<collection>.find()`

  ​	- 查询当前集合中的所有的文档

# 2 Node.js中操作MongoDB

使用[mongoose](http://www.mongoosejs.net/docs/index.html)

方式一：

```
// 引入mongoose
const mongoose = require('mongoose');
// 连接数据库
mongoose.connect('mongodb://localhost:27017/test', { useNewUrlParser: true, useUnifiedTopology: true });
// 定义一个表的结构
const Goods = mongoose.model('Goods', { goodsName: String, price: String, number: String });
//添加表的数据
const iphoneObj = new Goods({ goodsName: 'iPhone', price: '￥:3000', number: String });
// 返回的结果是一个 promise 对象
iphoneObj.save().then((data) => {
    console.log('insert ok!', data);
    mongoose.disconnect(); //断开数据库。默认情况下，操作完毕后，客户端不会和服务器断开
})
```

数据的增加，方式二：

```
// 引入mongoose
const mongoose = require('mongoose');
// 连接数据库
mongoose.connect('mongodb://localhost:27017/test', { useNewUrlParser: true, useUnifiedTopology: true });

// 表字段的描述信息
const MemberSchema = mongoose.Schema({
    username: String,
    password: String,
    age: Number,
});

// 数据表（数据模型 model）
//注意：在定义表名后，在数据库建表的时候，会自动的吧表转换为小写，同时改为表名单词的复数形式，例如members
const MemberModel = mongoose.model('Member', MemberSchema)
    // 数据增加
    // 1.准备数据
    //后期此处数据都是前端通过表单提交过来的
const member1 = new MemberModel({
    username: 'alex',
    password: 'admin',
    age: 18,
});

// 插入数据
const p1 = member1.save();
p1.then(data => {
    // 成功的回调
    console.log('insert ok!', data);
    mongoose.disconnect(); //断开数据库。默认情况下，操作完毕后，客户端不会和服务器断开
}, error => {
    // 失败的回调
    console.log('insert failure!', error);
    mongoose.disconnect();
});
```

数据的查看：方式一：使用promise封装

```
function findData() {
    return new Promise((resolve, reject) => {
        // 数据的查看
        MenberModel.find((error, data) => {
            if (error) {
                reject(error);
                return;
            }
            resolve(data);
        });
    })
}
async function run() {
    let data = await findData();
    console.log(data);
}

run();
```

数据的查看：方式二：不使用promise封装

```
// 数据的查看
async function run() {
    try {
        let data = await MenberModel.find();
        console.log(data);
    } catch (error) {
        console.warn(error);
    } finally {
        console.log('无论正确与否都会执行')
        // 断开数据库
        mongoose.disconnect();
    }
}

run();
```

数据的删除：

```
// 数据的删除
// 注意：现在监视使用deleteOne代替remove
MenberModel.deleteOne({ username: 'alex' }, function(error) {
    if (error) {
        console.log(error);
        return
    }
    console.log('删除成功了！！！')
    mongoose.disconnect();
});
```

数据的更新：

```
let _id='611c6eca0c024b29d0d35da9'
// 数据的更新
MenberModel.find(_id, (error, data) => {
    if (error) {
        console.log(error);
        return
    }
    console.log(data);
    data.age = 32; //完成修改
    console.log('修改成功了！！！')
    mongoose.disconnect();
});
```



