# Ajax

## 1.1 AJAX简介

AJAX 全称为Asynchronous JavaScript And XML ，就是异步的 JS 和 XML 。

通过 AJAX 可以在浏览器中向服务器发送异步请求，最大的优势：无刷新获取数据。

AJAX 不是新的编程语言，而是一种将现有的标准组合在一起使用的新方式。

## 1.2 XML简介

XML 可扩展标记语言。

XML 被设计用来传输和存储数据。（而HTML是用来呈现数据）

XML 和 HTML 类似，不同的是HTML中都是预定义标签，而XML中没有预定义标签，全都是自定义标签，用来表示一些数据。

![image-20210811215811337](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210811215811337.png)

```xml
<student>
	<name>孙悟空</name>
	<age>18</age>
	<gender>男</gender>
</student>
```

现在已经被JSON取代了。

```json
{"name":"孙悟空","age":18,"gender":"男"}
```

## 1.3 AJAX 的特点

### 1.3.1 AJAX 的优点

1. 可以无须刷新页面与服务器端进行通信。

2. 允许你根据用户事件来更新部分页面内容。

### 1.3.2 AJAX 的缺点

1. 没有浏览历史，不能回退
2. 存在跨域问题（同源）
3. SEO不友好（SEO指搜索引擎优化）（Ajax异步请求的数据不能被爬虫获取）

## 1.4 HTTP 协议

HTTP （hypertext transport protocol）协议【超文本传输协议】，协议详细规定了浏览器和万维网服务器之间互相通信的规则。

约定的规则包括请求和响应：

请求报文：

![image-20210811221816169](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210811221816169.png)

响应报文：

![image-20210811222008640](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210811222008640.png)

常见状态码：

403 被禁止

200 OK 请求成功。一般用于GET 与POST 请求
201 Created 已创建。成功请求并创建了新的资源
401 Unauthorized 未授权/请求要求用户的身份认证
404 Not Found 服务器无法根据客户端的请求找到资源
500 Internal Server Error 服务器内部错误，无法完成请求



## 1.5 安装node.js

### 1.5.1 Express服务端框架

Express基于node.js

1.安装node.js

2.安装Express：在vscode中空白区域单击鼠标右键，打开终端，如下图：

![image-20210812091839366](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210812091839366.png)

初始化：npm init --yes

![image-20210812091920514](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210812091920514.png)

安装Express：npm i express

![image-20210812092204105](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210812092204105.png)

引入Express：

![image-20210812092350013](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210812092350013.png)

创建应用对象：

![image-20210812092408440](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210812092408440.png)

创建路由规则：

![image-20210812092457899](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210812092457899.png)

示范：设置响应：

![image-20210812092557700](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210812092557700.png)

监听端口启动服务：

![image-20210812092518084](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210812092518084.png)

切换到当前目录下：cd 当前目录的路径

启动：node 文件名.js

![image-20210812092637168](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210812092637168.png)

结果：

![image-20210812092740340](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210812092740340.png)



如果端口被占用：

![image-20210812101305568](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210812101305568.png)



点击终端右侧的node项：

![image-20210812101411470](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210812101411470.png)

按Ctrl+c关闭占用端口的服务：

![image-20210812101529694](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210812101529694.png)

最后重新启动要打开的服务：node 文件名.js



## 报错

**问题：**“http://127.0.0.1:8000/server”上对XMLHttpRequest的访问被CORS策略阻止:被请求的资源上没有“Access- control - allow- origin”头。

![image-20210813094056822](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210813094056822.png)

**原因：**跨域问题

**解决方法一：**

1. （1）首先通过命令行 npm install -g cnpm --registry=https://registry.npm.taobao.org， 安装cnpm。

2. 然后在server.js文件中，const express = require(express);之后添加 const cors=require('cors');  注意cors是由引号包围的

3. 在const app = express();之后添加app.use(cors()); 

4. 启动运行，即可不报错

**解决方法二：**

在火狐运行的话请求头能传过去，在chrom浏览器就会报错，经过几番测试，找到问题就是js文件里的setHeaders(Access-Control-Allow-Headers, *);这句，chrom识别不了自定义请求头name和password，也就是html文件里headers:{name:admin,password:admin}这段。
解决方法：在/axinos-server代码块里添加一句response.setHeader(Access-Control-Allow-Headers, name,password);让chrom浏览器能够识别自定义请求头，不报错



### 使用fetch()函数发送AJAX请求

fetch()属于全局对象，可以直接调用，返回的结果是一个promise对象。

# 3 跨域

## 3.1 同源策略

同源策略（Same-Origin-Policy)

当前网页的URL和Ajax请求的URL两者之间必须协议、域名、端口，必须完全相同，Ajax默认是遵循同源策略的
